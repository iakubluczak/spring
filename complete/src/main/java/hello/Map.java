package com.example;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class Map <K,V> implements Map<K, V>{
	
	private ArrayList<K> keys;
    private ArrayList<V> values;
	

    public V get(Object key) {
        int i = keys.indexOf(key);
        if (i == -1)
            return null;
        return values.get(i);
    }
    
    public boolean containsKey(Object key) {
        return keys.contains(key);
    }
    
    public Set<K> keySet() {
        return new TreeSet<K>(keys);
    }
 
    //Dodaj par� key-value do mapy
    @SuppressWarnings("unchecked")
    @Override
    public V put(K key, V value) {
        for (int i=0; i<keys.size(); i++) {
            V oldValue = values.get(i);
            if(((Comparable<K>) key).compareTo(keys.get(i)) == 0) {
                values.set(i, value);
                return oldValue;
            }
        }
        keys.add(key);
        values.add(value);
        return null;
    }
    
    public V remove(Object key) {
        int i = keys.indexOf(key);
        if ( i == -1 )
            return null;
        V value = values.get(i);
        keys.remove(i);
        values.remove(i);
        return value;
    }
    
    public int size() {
        return keys.size();
    }
}

